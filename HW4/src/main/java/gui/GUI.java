package gui;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.JScrollPane;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.CardLayout;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import model.Account;
import model.Person;
import javax.swing.SwingConstants;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTable table1;
	private JPanel accountPanel;
	private JPanel persPanel;
	private JPanel opPanel;
	private JPanel menu;
	private JButton btnAccounts;
	private JButton btnPersons;
	private JButton btnOperation;
	private JButton btnMenu;
	private JButton btnInsertPerson;
	private JButton btnUpdatePerson;
	private JTextField textName;
	private JTextField textPhone;
	private JButton btnDeletePerson;
	private JLabel lblMonth;
	private JTextField textMonth;
	private JLabel lblPrice;
	private JTextField textYear;
	private JButton btnInsertAccount;
	private JButton btnDeleteAccount;
	private JButton btnUpdateAccount;
	private JButton buttonMenu1;
	private JComboBox<Person> comboBox_2;
	private JLabel lblNewLabel;
	private JComboBox<Account> comboBox_3;
	private JLabel lblSelectAProduct;
	private JTextField textField;
	private JLabel lblNewLabel_1;
	private JButton btnDeposit;
	private JButton buttonMenu2;
	private JLabel lblToMakeAn;
	private JLabel lblAndIntroduceA;
	private JScrollPane scrollPane;
	private JScrollPane scrollPane1;
	private JComboBox<Person> comboBox;
	private JButton btnRedraw;
	private JButton btnInsertAccountSpending;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 997, 1071);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		menu = new JPanel();
		contentPane.add(menu, "name_180132812520410");
		menu.setLayout(null);
		setTitle("Bank");
		
		btnAccounts = new JButton("Accounts");
		btnAccounts.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnAccounts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnAccounts.setBounds(568, 256, 255, 61);
		menu.add(btnAccounts);
		
		btnPersons = new JButton("Persons");
		btnPersons.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnPersons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnPersons.setBounds(128, 256, 260, 61);
		menu.add(btnPersons);
		
		btnOperation = new JButton("Make operation");
		btnOperation.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnOperation.setBounds(333, 381, 293, 77);
		menu.add(btnOperation);
		
		persPanel = new JPanel();
		contentPane.add(persPanel, "name_180132824672168");
		persPanel.setLayout(null);
		
		btnMenu = new JButton("Menu");
		btnMenu.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnMenu.setBounds(379, 896, 235, 62);
		persPanel.add(btnMenu);
		
		btnInsertPerson = new JButton("Insert Person");
		btnInsertPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnInsertPerson.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnInsertPerson.setBounds(75, 706, 322, 62);
		persPanel.add(btnInsertPerson);
		
		btnUpdatePerson = new JButton("Update Person");
		btnUpdatePerson.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnUpdatePerson.setBounds(334, 806, 322, 62);
		persPanel.add(btnUpdatePerson);
		
		textName = new JTextField();
		textName.setFont(new Font("Tahoma", Font.PLAIN, 30));
		textName.setColumns(10);
		textName.setBounds(219, 621, 236, 39);
		persPanel.add(textName);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblName.setBounds(288, 583, 115, 33);
		persPanel.add(lblName);
		
		textPhone = new JTextField();
		textPhone.setFont(new Font("Tahoma", Font.PLAIN, 30));
		textPhone.setColumns(10);
		textPhone.setBounds(480, 621, 236, 39);
		persPanel.add(textPhone);
		
		JLabel lblPhone = new JLabel("Phone:");
		lblPhone.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblPhone.setBounds(541, 583, 115, 33);
		persPanel.add(lblPhone);
		
		btnDeletePerson = new JButton("Delete Person");
		btnDeletePerson.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnDeletePerson.setBounds(592, 706, 322, 62);
		persPanel.add(btnDeletePerson);
		
		accountPanel = new JPanel();
		contentPane.add(accountPanel, "name_180132836834188");
		accountPanel.setLayout(null);
		
		lblMonth = new JLabel("Month:");
		lblMonth.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblMonth.setBounds(187, 575, 115, 33);
		accountPanel.add(lblMonth);
		
		textMonth = new JTextField();
		textMonth.setFont(new Font("Tahoma", Font.PLAIN, 30));
		textMonth.setColumns(10);
		textMonth.setBounds(124, 613, 236, 39);
		accountPanel.add(textMonth);
		
		lblPrice = new JLabel("Year:");
		lblPrice.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblPrice.setBounds(443, 575, 115, 33);
		accountPanel.add(lblPrice);
		
		textYear = new JTextField();
		textYear.setFont(new Font("Tahoma", Font.PLAIN, 30));
		textYear.setColumns(10);
		textYear.setBounds(373, 612, 236, 39);
		accountPanel.add(textYear);
		
		btnInsertAccount = new JButton("Insert Account Savings");
		btnInsertAccount.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnInsertAccount.setBounds(26, 708, 450, 62);
		accountPanel.add(btnInsertAccount);
		
		btnDeleteAccount = new JButton("Delete Account");
		btnDeleteAccount.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnDeleteAccount.setBounds(81, 806, 322, 62);
		accountPanel.add(btnDeleteAccount);
		
		btnUpdateAccount = new JButton("Update Account");
		btnUpdateAccount.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnUpdateAccount.setBounds(568, 806, 322, 62);
		accountPanel.add(btnUpdateAccount);
		
		buttonMenu1 = new JButton("Menu");
		buttonMenu1.setFont(new Font("Tahoma", Font.PLAIN, 34));
		buttonMenu1.setBounds(373, 896, 235, 62);
		accountPanel.add(buttonMenu1);
		
		JLabel lblPerson = new JLabel("Person:");
		lblPerson.setBounds(683, 578, 115, 33);
		accountPanel.add(lblPerson);
		
		btnInsertAccountSpending = new JButton("Insert Account Spending");
		btnInsertAccountSpending.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnInsertAccountSpending.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnInsertAccountSpending.setBounds(487, 708, 442, 62);
		accountPanel.add(btnInsertAccountSpending);
		
		opPanel = new JPanel();
		contentPane.add(opPanel, "name_180132848306675");
		opPanel.setLayout(null);
		
		
		
		lblNewLabel = new JLabel("Select a person:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblNewLabel.setBounds(115, 315, 322, 52);
		opPanel.add(lblNewLabel);
		
		
		lblSelectAProduct = new JLabel("Select an account:");
		lblSelectAProduct.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblSelectAProduct.setBounds(590, 315, 322, 52);
		opPanel.add(lblSelectAProduct);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 34));
		textField.setBounds(115, 570, 236, 40);
		opPanel.add(textField);
		textField.setColumns(10);
		
		lblNewLabel_1 = new JLabel("Sum:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblNewLabel_1.setBounds(183, 498, 193, 102);
		opPanel.add(lblNewLabel_1);
		
		btnDeposit = new JButton("Deposit");
		btnDeposit.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnDeposit.setBounds(168, 660, 236, 95);
		opPanel.add(btnDeposit);
		
		buttonMenu2 = new JButton("Menu");
		buttonMenu2.setFont(new Font("Tahoma", Font.PLAIN, 34));
		buttonMenu2.setBounds(416, 808, 235, 95);
		opPanel.add(buttonMenu2);
		
		lblToMakeAn = new JLabel("To make an operation, select a person, an account");
		lblToMakeAn.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblToMakeAn.setBounds(88, 70, 867, 52);
		opPanel.add(lblToMakeAn);
		
		lblAndIntroduceA = new JLabel("and introduce a sum and a date greater than the current date:");
		lblAndIntroduceA.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblAndIntroduceA.setBounds(0, 136, 955, 52);
		opPanel.add(lblAndIntroduceA);
		
		btnRedraw = new JButton("Redraw");
		btnRedraw.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnRedraw.setBounds(659, 660, 236, 95);
		opPanel.add(btnRedraw);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(26, 16, 903, 451);
		scrollPane.createVerticalScrollBar();
		
		scrollPane1 = new JScrollPane();
		scrollPane1.setBounds(26, 16, 903, 451);
		scrollPane1.createVerticalScrollBar();
		
		comboBox_3 = new JComboBox();
		comboBox_3.setFont(new Font("Tahoma", Font.PLAIN, 34));
		comboBox_3.setBounds(541, 379, 371, 67);
		
		comboBox_2 = new JComboBox();
		comboBox_2.setFont(new Font("Tahoma", Font.PLAIN, 34));
		comboBox_2.setBounds(84, 379, 351, 67);
		
		comboBox = new JComboBox();
		accountPanel.add(comboBox);
		opPanel.add(comboBox_2);
		opPanel.add(comboBox_3);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 30));
		textField_1.setColumns(10);
		textField_1.setBounds(416, 573, 236, 39);
		opPanel.add(textField_1);
		
		JLabel label = new JLabel("Month:");
		label.setFont(new Font("Tahoma", Font.PLAIN, 34));
		label.setBounds(478, 533, 115, 33);
		opPanel.add(label);
		
		JLabel label_1 = new JLabel("Year:");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 34));
		label_1.setBounds(779, 533, 115, 33);
		opPanel.add(label_1);
		
		textField_2 = new JTextField();
		textField_2.setFont(new Font("Tahoma", Font.PLAIN, 30));
		textField_2.setColumns(10);
		textField_2.setBounds(709, 570, 236, 39);
		opPanel.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setFont(new Font("Tahoma", Font.PLAIN, 34));
		textField_3.setEditable(false);
		textField_3.setHorizontalAlignment(SwingConstants.CENTER);
		textField_3.setBounds(551, 474, 236, 39);
		opPanel.add(textField_3);
		textField_3.setColumns(10);
		
		
		table = new JTable();
		table.setFont(new Font("Tahoma", Font.PLAIN, 28));
		table.setRowHeight(100);
		table.doLayout();
		scrollPane.setViewportView(table);
		persPanel.add(scrollPane);
		
		table1 = new JTable();
		table1.setFont(new Font("Tahoma", Font.PLAIN, 28));
		table1.setRowHeight(100);
		table1.doLayout();
		scrollPane1.setViewportView(table1);
		accountPanel.add(scrollPane1);
		
		textField_4 = new JTextField();
		textField_4.setHorizontalAlignment(SwingConstants.CENTER);
		textField_4.setFont(new Font("Tahoma", Font.PLAIN, 34));
		textField_4.setEditable(false);
		textField_4.setColumns(10);
		textField_4.setBounds(253, 512, 236, 39);
		accountPanel.add(textField_4);
		
	}
	
	public void setTextOp(String s)
	{
		textField_3.setText(s);
	}
	
	public void setTextAcc(String s)
	{
		textField_4.setText(s);
	}
	
	public void setComboBoxPersAcc(List<Person> persons)
	{
		accountPanel.remove(comboBox);
		comboBox = new JComboBox(persons.toArray());
		comboBox.setBounds(635, 613, 198, 39);
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 34));
		accountPanel.add(comboBox);
	}
	
	public void setComboBoxPersons(List<Person> persons)
	{

		DefaultComboBoxModel m = new DefaultComboBoxModel(persons.toArray()); 
		comboBox_2.setModel(m);
	}
	
	public void addComboBoxListener(ActionListener a)
	{
		comboBox_2.addActionListener(a);
	}
	
	public void addComboBoxListenerAccounts(ActionListener a)
	{
		comboBox_3.addActionListener(a);
	}
	
	public void setPerm(boolean b)
	{
		textField.setEnabled(b);
	}
	
	public void setComboBoxAccounts(List<Account> accounts)
	{
		DefaultComboBoxModel m = new DefaultComboBoxModel(accounts.toArray()); 
		comboBox_3.setModel(m);
		
	}
	
	public void setTableAccounts(DefaultTableModel t)
	{
		table1.setModel(t);
	}
	
	public void setTablePersons(DefaultTableModel t)
	{
		table.setModel(t);
	}
	
	public int getMonthOp()
	{
		return Integer.parseInt(textField_1.getText());
	}
	
	public int getYearOp()
	{
		return Integer.parseInt(textField_2.getText());
	}
	
	
	public void addOrder(ActionListener a)
	{
		btnOperation.addActionListener(a);
	}
	
	public void addPerson(ActionListener a)
	{
		btnPersons.addActionListener(a);
	}
	
	public void addAccount(ActionListener a)
	{
		btnAccounts.addActionListener(a);
	}
	
	public void addMenu(ActionListener a)
	{
		btnMenu.addActionListener(a);
	}
	
	public void addMenu1(ActionListener a)
	{
		buttonMenu1.addActionListener(a);
	}
	
	public void addMenu2(ActionListener a)
	{
		buttonMenu2.addActionListener(a);
	}
	
	public void addDeposit(ActionListener a)
	{
		btnDeposit.addActionListener(a);
	}
	
	public void addRedraw(ActionListener a)
	{
		btnRedraw.addActionListener(a);
	}
	
	public void addInsertPerson(ActionListener a)
	{
		btnInsertPerson.addActionListener(a);
	}
	
	public void addDeletePerson(ActionListener a)
	{
		btnDeletePerson.addActionListener(a);
	}
	
	public void addUpdatePerson(ActionListener a)
	{
		btnUpdatePerson.addActionListener(a);
	}
	
	
	public void addInsertAccount(ActionListener a)
	{
		btnInsertAccount.addActionListener(a);
	}
	
	public void addInsertAccountSpending(ActionListener a)
	{
		btnInsertAccountSpending.addActionListener(a);
	}
	
	public void addDeleteAccount(ActionListener a)
	{
		btnDeleteAccount.addActionListener(a);
	}
	
	public void addUpdateAccount(ActionListener a)
	{
		btnUpdateAccount.addActionListener(a);
	}
	
	
	public void setMenu(boolean b)
	{
		menu.setVisible(b);
	}
	
	public void setOrdPanel(boolean b)
	{
		opPanel.setVisible(b);
	}
	
	public void setProdPanel(boolean b)
	{
		accountPanel.setVisible(b);
	}
	
	public void setCustPanel(boolean b)
	{
		persPanel.setVisible(b);
	}
	

	
	public String getPersName()
	{
		return textName.getText();
	}
	

	
	public String getPhone()
	{
		return textPhone.getText();
	}
	
	
	
	public Person getPerson()
	{
		Person p = (Person) comboBox_2.getSelectedItem();
		return p;
	}
	
	public Account getAccount()
	{
		Account a = (Account) comboBox_3.getSelectedItem();
		return a;
	}
	
	public Person getPersonAcc()
	{
		Person p = (Person) comboBox.getSelectedItem();
		return p;
	}
	
	
	public String getMonth() throws Exception
	{
		if (Integer.parseInt(textMonth.getText()) < 1 || Integer.parseInt(textMonth.getText()) > 12)
			throw new Exception();
		return textMonth.getText();
	}
	
	public String getYear() throws Exception
	{
		if(textYear.getText().length() != 4)
			throw new Exception();
		return textYear.getText();
	}
	
	
	public int getSum()
	{
		return Integer.parseInt(textField.getText());
	}
	
	public Person getSelectedRowPersons()
	{
		Person p = new Person(((Integer) table.getValueAt(table.getSelectedRow(), 0)).intValue(), (String)table.getValueAt(table.getSelectedRow(), 1), (String)table.getValueAt(table.getSelectedRow(), 2));
		p.setOp((String) table.getValueAt(table.getSelectedRow(), 3));
		System.out.println(p.getName() + " " + p.getPhoneNo() + " " + p.getLastOperation());
		return p;
	}
	
	public Account getSelectedRowAccount()
	{
		Account a = new Account((Integer) table1.getValueAt(table1.getSelectedRow(), 0),
				Integer.parseInt((table1.getValueAt(table1.getSelectedRow(), 1).toString())), 
				Integer.parseInt(table1.getValueAt(table1.getSelectedRow(), 2).toString()));
		a.setDateLastAccess(Integer.parseInt(table1.getValueAt(table1.getSelectedRow(), 3).toString()),
				Integer.parseInt((String) table1.getValueAt(table1.getSelectedRow(), 4).toString()));
		return a;
	}
}
