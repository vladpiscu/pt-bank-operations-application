package HW4.HW4;

import gui.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        GUI g = new GUI();
        g.setVisible(true);
        Controller c = new Controller(g);
    }
}
