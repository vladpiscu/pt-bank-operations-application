package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import model.*;
import bank.*;

public class Controller {
	private GUI g;
	private Bank b;
	private int persId;
	private int month;
	private int year;
	
	public Controller(GUI g) {
		this.g = g;
		b = new Bank();
		persId = 0;
		month = 0;
		year = 0;
		List<Person> pers = b.personToList();
		for(int i = 0; i < b.getPersonSize(); i++)
		{
			if(persId <= pers.get(i).getId())
				persId = pers.get(i).getId() + 1;
			List<Account> acc = b.accountsToList(pers.get(i));
			for(Account a : acc)
			{
				if(year < a.getYearLastAccess())
				{
					month = a.getMonthLastAccess();
					year = a.getYearLastAccess();
				}
				else
				{
					if(month < a.getMonthLastAccess() && year == a.getYearLastAccess())
						month = a.getMonthLastAccess();
				}
				
			}
		}
		this.g.addOrder(new Ord());
		this.g.addPerson(new Pers());
		this.g.addAccount(new Acc());
		this.g.addMenu(new Menu());
		this.g.addMenu1(new Menu());
		this.g.addMenu2(new Menu());
		this.g.addInsertPerson(new InsertPers());
		this.g.addDeletePerson(new DeletePers());
		this.g.addUpdatePerson(new UpdateCust());
		this.g.addInsertAccount(new InsertAccSavings());
		this.g.addInsertAccountSpending(new InsertAccSpending());
		this.g.addComboBoxListener(new RefreshBox());
		this.g.addDeleteAccount(new DeleteAcc());
		this.g.addUpdateAccount(new UpdateAcc());
		this.g.addDeposit(new Deposit());
		this.g.addRedraw(new Redraw());
		this.g.addComboBoxListenerAccounts(new RefreshPerm());
	}
	
	public class RefreshPerm implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			if(g.getAccount() instanceof SavingsAccount)
			{
				if(((SavingsAccount) g.getAccount()).canRedraw())
					g.setPerm(false);
				else
					g.setPerm(true);
			}
			else
			{
				g.setPerm(true);
			}
		}
	}
	
	public class Deposit implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			try{
				int iYear = g.getYearOp();
				int iMonth = g.getMonthOp();
				if(iYear < year || (iYear == year && iMonth < month))
					throw new Exception();
				year = iYear;
				month = iMonth;
				g.setTextOp(month + "/" + year);
				g.setTextAcc(month + "/" + year);
				g.getAccount().deposit(1.0 * g.getSum(), g.getMonthOp(), g.getYearOp());
				if(g.getAccount() instanceof SavingsAccount)
				{
					if(((SavingsAccount) g.getAccount()).canRedraw())
						g.setPerm(false);
					else
						g.setPerm(true);
				}
				else
				{
					g.setPerm(true);
				}
				b.serialize();
			}
			catch(Exception e)
			{
				JOptionPane.showMessageDialog(null, "Input is incorect!");
			}
		}
	}
	
	public class Redraw implements ActionListener
	{
		
		public void actionPerformed(ActionEvent arg0) {
			try{
				int iYear = g.getYearOp();
				int iMonth = g.getMonthOp();
				if(iYear < year || (iYear == year && iMonth < month))
					throw new Exception();
				year = iYear;
				month = iMonth;
				g.setTextOp(month + "/" + year);
				g.setTextAcc(month + "/" + year);
				if(g.getAccount() instanceof SavingsAccount)
					g.getAccount().redraw(g.getAccount().getBalance(iMonth, iYear, false), g.getMonthOp(), g.getYearOp());
				else
					g.getAccount().redraw(1.0 * g.getSum(), g.getMonthOp(), g.getYearOp());
				if(g.getAccount() instanceof SavingsAccount)
				{
					if(((SavingsAccount) g.getAccount()).canRedraw())
						g.setPerm(false);
					else
						g.setPerm(true);
				}
				else
				{
					g.setPerm(true);
				}
				b.serialize();
			}
			catch(Exception e)
			{
				JOptionPane.showMessageDialog(null, "Input is incorect!");
			}
			
		}
	}
	
	
	public class UpdateAcc implements ActionListener
	{
		public Account getAccount()
		{
			try{
				Account a = g.getSelectedRowAccount();
				
				return a;
			}
			catch(Exception e)
			{
				JOptionPane.showMessageDialog(null, "Input is incorect!");
			}
			return null;
		}
		
		public void actionPerformed(ActionEvent arg0) {
			System.out.println(g.getSelectedRowAccount().getAccoundId());
			b.updateAccount(this.getAccount());
	    	g.setTablePersons(b.createTablePerson());
		}
	}
	
	public class RefreshBox implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			g.setComboBoxAccounts(b.accountsToList(g.getPerson()));
		}
	}
	
	public class DeleteAcc implements ActionListener
	{
		
		public void actionPerformed(ActionEvent arg0) {
			Account a = g.getSelectedRowAccount();
			List<Person> pers = b.personToList();
			Person person = pers.get(0);
			for(Person p : pers)
			{
				List<Account> acc = b.accountsToList(p);
				if(acc.contains((Object) a))
					person = p;
			}
			System.out.println(person.toString() + " with account " + a.getAccoundId());
			b.deleteAccount(a, person);
	    	g.setTableAccounts(b.createTableAccount());
		}
	}
	
	public class InsertAccSpending implements ActionListener
	{
		public void addAccount()
		{
			try{
				int iYear = Integer.parseInt(g.getYear());
				int iMonth = Integer.parseInt(g.getMonth());
				if(iYear < year || (iYear == year && iMonth < month))
					throw new Exception();
				year = iYear;
				month = iMonth;
				g.setTextOp(month + "/" + year);
				g.setTextAcc(month + "/" + year);
				b.addNewSpendingAcound(g.getPersonAcc(), iMonth, iYear);
			}
			catch(Exception e)
			{
				JOptionPane.showMessageDialog(null, "Input is incorect!");
			}
		}
		
		public void actionPerformed(ActionEvent arg0) {
			this.addAccount();
	    	g.setTableAccounts(b.createTableAccount());
		}
	}
	
	public class InsertAccSavings implements ActionListener
	{
		public void addAccount()
		{
			try{
				int iYear = Integer.parseInt(g.getYear());
				int iMonth = Integer.parseInt(g.getMonth());
				if(iYear < year || (iYear == year && iMonth < month))
					throw new Exception();
				year = iYear;
				month = iMonth;
				g.setTextOp(month + "/" + year);
				g.setTextAcc(month + "/" + year);
				b.addNewSavingsAccount(g.getPersonAcc(), iMonth, iYear);
			}
			catch(Exception e)
			{
				JOptionPane.showMessageDialog(null, "Input is incorect!");
			}
		}
		
		public void actionPerformed(ActionEvent arg0) {
			this.addAccount();
	    	g.setTableAccounts(b.createTableAccount());
		}
	}
	
	
	
	public class UpdateCust implements ActionListener
	{
		public Person getPerson()
		{
			try{
				Person p = g.getSelectedRowPersons();
				if(p.getPhoneNo().length() != 10)
					throw new Exception();
				return p;
			}
			catch(Exception e)
			{
				JOptionPane.showMessageDialog(null, "Input is incorect!");
			}
			return null;
		}
		
		public void actionPerformed(ActionEvent arg0) {
			b.updatePerson(this.getPerson());
	    	g.setTablePersons(b.createTablePerson());
		}
	}
	
	public class DeletePers implements ActionListener
	{
		
		public void actionPerformed(ActionEvent arg0) {
			Person p = g.getSelectedRowPersons();
			b.removePerson(p);
	    	g.setTablePersons(b.createTablePerson());
		}
	}
	
	public class InsertPers implements ActionListener
	{
		
		public void actionPerformed(ActionEvent arg0) {
			try{
				if(g.getPhone().length() != 10)
					throw new Exception();
				Person p = new Person(persId, g.getPersName(), g.getPhone());
				persId++;
				b.addPerson(p);
				g.setTablePersons(b.createTablePerson());
			}
			catch(Exception e)
			{
				JOptionPane.showMessageDialog(null, "Input is incorect!");
			}
		}
	}
	
	public class Menu implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			g.setOrdPanel(false);
			g.setCustPanel(false);
			g.setProdPanel(false);
			g.setMenu(true);
		}
	}
	
	public class Ord implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			g.setOrdPanel(true);
			g.setCustPanel(false);
			g.setProdPanel(false);
			g.setMenu(false);
			g.setComboBoxPersons(b.personToList());
			g.setComboBoxAccounts(b.accountsToList(b.personToList().get(0)));
			g.setTextOp(month + "/" + year);
			g.setTextAcc(month + "/" + year);
			if(g.getAccount() instanceof SavingsAccount)
			{
				if(((SavingsAccount) g.getAccount()).canRedraw())
					g.setPerm(false);
				else
					g.setPerm(true);
			}
			else
			{
				g.setPerm(true);
			}
		}
	}
	
	public class Pers implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			g.setOrdPanel(false);
			g.setCustPanel(true);
			g.setProdPanel(false);
			g.setMenu(false);
	    	g.setTablePersons(b.createTablePerson());
		}
	}
	
	public class Acc implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			g.setOrdPanel(false);
			g.setCustPanel(false);
			g.setProdPanel(true);
			g.setMenu(false);
			g.setComboBoxPersAcc(b.personToList());
			g.setTextOp(month + "/" + year);
			g.setTextAcc(month + "/" + year);
	    	g.setTableAccounts(b.createTableAccount());
		}
	}
}
