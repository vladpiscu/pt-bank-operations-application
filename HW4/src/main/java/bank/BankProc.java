package bank;

import java.util.List;

import model.Account;
import model.Person;

public interface BankProc {
	
	
	/**
	 *Adds a new person p
	 *@pre p != null && personExist(p) == false
	 *@post getPersonSize() == getPersonSize()@pre + 1
	 */
	public void addPerson(Person p);
	
	/**
	 * Removes the person p
	 * @pre p != null && personExist(p) == true
	 * @post getPersonSize() == getPersonSize()@pre - 1
	 */
	public void removePerson(Person p);
	
	/**
	 * Returns a list of persons
	 * @pre true
	 * @post @nochange
	 */
	public List<Person> personToList();
	
	/**
	 * Adds a new savings account to the person p
	 * @pre p != null && personExist(p) == true
	 * @post getAccountSize(p) == getAcountSize(p)@pre + 1
	 */
	public void addNewSavingsAccount(Person p, int month, int year);
	
	/**
	 * Adds a new spending account to the person p
	 * @pre p != null && personExist(p) == true
	 * @post getAccountSize(p) == getAcountSize(p)@pre + 1 
	 */
	public void addNewSpendingAcound(Person p, int month, int year);
	
	/**
	 * Deletes the account a from person p
	 * @pre p != null && a != null && personExist(p) == true && accountExist(a) == true
	 * @post getAccountSize(p) == getAcountSize(p)@pre - 1
	 */
	public void deleteAccount(Account a, Person p);
	
	/**
	 * Returns a list of accounts from a person p
	 * @pre true
	 * @post #nochange
	 */
	public List<Account> accountsToList(Person p);
	
	/**
	 * Returns true if the person exists
	 * @pre p != null
	 * @post @nochange
	 */
	public boolean personExists(Person p);
	
	/**
	 * Returns true if the account exists
	 * @pre a != null && p != null
	 * @post @nochange
	 */
	public boolean accountExists(Account a, Person p);
	
	/**
	 * returns the # of persons
	 * @pre true
	 * @post @nochange
	 */
	public int getPersonSize();
	
	/**
	 * return the # of accounts that the person p has
	 * @pre p != null && personExists(p)
	 * @post @nochange
	 */
	public int getAccountSize(Person p);
	
}
