package model;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;

public class Person implements Observer, Serializable{
	private int id;
	private String name;
	private String phoneNo;
	private String lastOperation;
	
	public Person(int id, String name, String phoneNo)
	{
		this.id = id;
		this.name = name;
		this.phoneNo = phoneNo;
		lastOperation = new String();
	}
	
	public int getId()
	{
		return id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String s)
	{
		name = s;
	}
	
	public void setPhoneNo(String s)
	{
		phoneNo = s;
	}
	
	
	public String getPhoneNo()
	{
		return phoneNo;
	}
	
	public String getLastOperation()
	{
		return lastOperation;
	}
	
	public boolean equals(Object o)
	{
		if(o instanceof Person)
		{
			if(((Person) o).getId() == this.id)
				return true;
		}
		return false;
	}
	
	public int hashCode()
	{
		return id;
	}
	
	public void setOp(String s)
	{
		lastOperation = s;
	}

	public void update(Observable arg0, Object arg1) {
		if(arg1 instanceof String)
		{
			lastOperation = ((String) arg1);
			JOptionPane.showMessageDialog(null, lastOperation);
		}
	}
	
	public String toString()
	{
		return name;
	}
}
