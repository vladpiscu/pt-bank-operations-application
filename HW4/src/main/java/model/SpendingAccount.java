package model;

public class SpendingAccount extends Account{

	private final int maintananceCost = 5;
	
	public SpendingAccount(int id, int month, int year)
	{
		super(id, month, year);
	}

	public double redraw(double sum, int month, int year) 
	{
		return super.redraw(sum + maintananceCost, month, year);
	}

	public double getBalance(int month, int year, boolean notify) 
	{
		if(notify == true)
			super.redraw(maintananceCost, month, year);
		return super.getBalance(month, year, notify);
	}

	public String toString()
	{
		return "SpendAcc with id:" + super.getAccoundId();
	}
}
