package bank;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import model.Account;
import model.Person;
import model.SavingsAccount;
import model.SpendingAccount;

/**
 * @invariant isWellFormed()
 */

public class Bank implements BankProc{
	private int accountsId;
	HashSet<Person> persons;
	HashMap<Person, List<Account>> personAccounts;
	
	public Bank()
	{
		persons = new HashSet<Person>();
		personAccounts = new HashMap<Person, List<Account>>();
		accountsId = 0;
		try{
			deserialize();
			for(Person p : persons)
			{
				List<Account> acc = this.accountsToList(p);
				for(Account a : acc)
				{
					if(a.getAccoundId() >= accountsId)
						accountsId = a.getAccoundId() + 1;
				}
			}
		}
		catch(Exception e)
		{
			System.out.println("Exceptie constructor");
		}
	}
	
	

	public void addPerson(Person p) {
		assert p!=null && personExists(p) == false : "pre addPerson unsatisfied";
		int size = persons.size();
		persons.add(p);
		List<Account> accounts = new ArrayList<Account>();
		personAccounts.put(p, accounts);
		assert isWellFormed() : "invariant unsatisfied";
		assert persons.size() == size + 1 : "post addPerson unsatisfied";
		try{
			serialize();
		}
		catch(Exception e)
		{
			System.out.println("Exceptie addPerson");
		}
	}

	public void removePerson(Person p) {
		assert p!=null && personExists(p) == true : "pre removePerson unsatisfied";
		int size = persons.size();
		persons.remove(p);
		personAccounts.remove(p);
		assert isWellFormed() : "invariant unsatisfied";
		assert persons.size() == size - 1 : "post removePerson unsatisfied";
		try{
			serialize();
		}
		catch(Exception e)
		{
			System.out.println("Exceptie removePerson");
		}
	}

	public List<Person> personToList() {
		List<Person> pers = new ArrayList<Person>();
		pers.addAll(persons);
		return pers;
	}

	public void addNewSavingsAccount(Person p, int month, int year) {
		assert p!=null && personExists(p)==true : "pre addNewSavingsAccount unsatisfied";
		SavingsAccount a = new SavingsAccount(accountsId, month, year);
		accountsId++;
		int size = getAccountSize(p);
		a.addObserver(p);
		personAccounts.get(p).add((Account) a);
		assert isWellFormed() : "invariant unsatisfied";
		assert getAccountSize(p) == size + 1: "post addNewSavingsAccount unsatisfied";
		try{
			serialize();
		}
		catch(Exception e)
		{
			System.out.println("Exceptie addAcount");
		}
	}

	public void addNewSpendingAcound(Person p, int month, int year) {
		assert p!=null && personExists(p)==true : "pre addNewSpendingsAccount unsatisfied";
		SpendingAccount a = new SpendingAccount(accountsId, month, year);
		accountsId++;
		a.addObserver(p);
		int size = getAccountSize(p);
		personAccounts.get(p).add((Account) a);
		assert isWellFormed() : "invariant unsatisfied";
		assert getAccountSize(p) == size + 1: "post addNewSpendingsAccount unsatisfied";
		try{
			serialize();
		}
		catch(Exception e)
		{
			System.out.println("Exceptie addAcount");
		}
	}

	public void deleteAccount(Account a, Person p) {
		assert a!=null && p!=null : "pre deleteAccount != null";
		assert personExists(p)==true : "pre deleteAccount personExists";
		assert accountExists(a, p)==true : "pre deleteAccount accountExists";
		int size = getAccountSize(p);
		personAccounts.get(p).remove((Object) a);
		assert isWellFormed() : "invariant unsatisfied";
		assert getAccountSize(p) == size - 1: "post deleteAccount unsatisfied";
		try{
			serialize();
		}
		catch(Exception e)
		{
			System.out.println("Exceptie deleteAccount");
		}
		
	}

	public List<Account> accountsToList(Person p) {
		List<Account> a = new ArrayList<Account>();
		a.addAll(personAccounts.get(p));
		return a;
	}

	public int getPersonSize() {
		return persons.size();
	}

	public int getAccountSize(Person p) {
		assert p!=null && personExists(p): "pre getAccountSize unsatisfied";
		return personAccounts.get(p).size();
	}

	public boolean personExists(Person p) {
		assert p!=null : "pre personExists unsatisfied";
		return persons.contains(p);
	}

	public boolean accountExists(Account a, Person p) {
		assert p!=null && a!=null : "pre accountExists unsatisfied";
		return personAccounts.get(p).contains(a);
	}
	
	public void updatePerson(Person p)
	{
		Iterator<Person> it = persons.iterator();
		while(it.hasNext())
		{
			Person pers = it.next();
			if(pers.equals(p))
			{
				pers.setName(p.getName());
				pers.setPhoneNo(p.getPhoneNo());
				pers.setOp(p.getLastOperation());
			}
		}
		try{
			serialize();
		}
		catch(Exception e)
		{
			System.out.println("Exceptie deleteAccount");
		}
	}
	
	public void updateAccount(Account a)
	{
		for(Person p : persons)
		{
			List<Account> acc = this.accountsToList(p);
			for(Account ac : acc)
			{
				if(ac.equals(a))
				{
					ac.setDateBegin(a.getMonthBegin(), a.getYearBegin());
					ac.setDateLastAccess(a.getMonthLastAccess(), a.getYearLastAccess());
				}
			}
		}
		try{
			serialize();
		}
		catch(Exception e)
		{
			System.out.println("Exceptie deleteAccount");
		}
	}
	
	protected boolean isWellFormed()
	{
		return persons.size() == personAccounts.size();
	}
	
	public DefaultTableModel createTablePerson()
	{
		JTable table = new JTable();
		
		
		DefaultTableModel model = new DefaultTableModel(){
			public boolean isCellEditable(int row,int column)  
	        {switch(column){             
	           case 0:  // select the cell you want make it not editable 
	             return false;
	           case 3:
	        	 return false;
	         default: return true;}  
		}};
		
		
		Object[] columnsName = new Object[4];
		columnsName[0] = (Object) new String("Id");
		columnsName[1] = (Object) new String("Name");
		columnsName[2] = (Object) new String("Phone");
		columnsName[3] = (Object) new String("Last operation");
		
		
		model.setColumnIdentifiers(columnsName);
		
		Object[] rowData = new Object[4];
		List<Person> pers = this.personToList();
		for(int i = 0; i < pers.size(); i++)
		{
			rowData[0] = (Object) pers.get(i).getId();
			rowData[1] = (Object) pers.get(i).getName();
			rowData[2] = (Object) pers.get(i).getPhoneNo();
			rowData[3] = (Object) pers.get(i).getLastOperation();
			model.addRow(rowData);
		}
		table.setModel(model);
		return model;
	}
	
	public DefaultTableModel createTableAccount()
	{
		JTable table = new JTable();
		DefaultTableModel model = new DefaultTableModel(){
			public boolean isCellEditable(int row,int column)  
	        {switch(column){             
	           	case 0:  // select the cell you want make it not editable 
	           		return false;
	           	case 5:
	           		return false;
	           	case 6:
	           		return false;
	         default: return true;}  
		}};
		Object[] columnsName = new Object[7];
		columnsName[0] = (Object) new String("Id");
		columnsName[1] = (Object) new String("MonthBegin");
		columnsName[2] = (Object) new String("YearBegin");
		columnsName[3] = (Object) new String("MonthLastAccess");
		columnsName[4] = (Object) new String("YearLastAccess");
		columnsName[5] = (Object) new String("Amount");
		columnsName[6] = (Object) new String("MainHolder");
		model.setColumnIdentifiers(columnsName);
		Object[] rowData = new Object[7];
		
		List<Person> pers = this.personToList();
		for(int i = 0; i < pers.size(); i++)
		{
			List<Account> accounts = this.accountsToList(pers.get(i));
			for(int j = 0; j < accounts.size(); j++)
			{
				rowData[0] = (Object) accounts.get(j).getAccoundId();
				rowData[1] = (Object) accounts.get(j).getMonthBegin();
				rowData[2] = (Object) accounts.get(j).getYearBegin();
				rowData[3] = (Object) accounts.get(j).getMonthLastAccess();
				rowData[4] = (Object) accounts.get(j).getYearLastAccess();
				rowData[5] = (Object) accounts.get(j).getBalance(accounts.get(j).getMonthLastAccess(), accounts.get(j).getYearLastAccess(), false);
				rowData[6] = (Object) pers.get(i).getName();
				model.addRow(rowData);
			}
		}
		table.setModel(model);
		return model;
	}
	
	public void serialize() throws IOException{
		FileOutputStream fileOut = new FileOutputStream("bank.ser");
		ObjectOutputStream out = new ObjectOutputStream(fileOut);
		out.reset();
		out.writeObject(persons);
		out.writeObject(personAccounts);
		out.flush();
		out.close();
		fileOut.close();
	}
	
	private void deserialize() throws IOException, ClassNotFoundException{
		FileInputStream fileIn = new FileInputStream("bank.ser");
		ObjectInputStream in = new ObjectInputStream(fileIn);
		persons = (HashSet<Person>) in.readObject();
		personAccounts = (HashMap<Person, List<Account>>) in.readObject();
		for(Person p : persons)
		{
			for(Account a : personAccounts.get(p))
				a.addObserver(p);
		}
		in.close();
		fileIn.close();
	}

}
