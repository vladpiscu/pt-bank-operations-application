package model;

import java.io.Serializable;
import java.util.Observable;

import javax.swing.JOptionPane;

public class Account extends Observable implements Serializable{
	private int id;
	private int monthBegin;
	private int yearBegin;
	private int monthLastAccess;
	private int yearLastAccess;
	private double amount;
	
	
	public Account(int id, int month, int year)
	{
		this.id = id;
		monthBegin = month;
		yearBegin = year;
		monthLastAccess = month;
		yearLastAccess = year;
		amount = 0;
	}
	
	public void setDateLastAccess(int month, int year)
	{
		monthLastAccess = month;
		yearLastAccess =year;
	}
	
	public void setQuantity(double q)
	{
		amount = q;
	}
	
	public boolean deposit(double sum, int month, int year) 
	{
		monthLastAccess = month;
		yearLastAccess = year;
		amount += sum;
		setChanged();
		notifyObservers(new String("DEPOSIT-accID: " + this.id +"\n on " + month + "/" + year + "\n.Balance: " + amount));
		return true;
	}
	
	public double redraw(double sum, int month, int year) 
	{
		monthLastAccess = month;
		yearLastAccess = year;
		if(sum <= amount)
		{
			if(this instanceof SavingsAccount)
			{
				setChanged();
				notifyObservers(new String("REDRAW " + ((SavingsAccount) this).getBalance(month, year, false) + "-accID:" + this.id +"\n on " + month + "/" + year + ".\nBalance: " + (amount - sum)));
			}
			else
			{
				setChanged();
				notifyObservers(new String("REDRAW " + sum + "-accID:" + this.id +"\n on " + month + "/" + year + ".\nBalance: " + (amount - sum)));
			}
			amount -= sum;
			return sum;
		}
		setChanged();
		notifyObservers(new String("Not enough money for REDRAW\n-accID" + this.id +".Balance: " + amount));
		return 0;
	}
	
	public double getBalance(int month, int year, boolean notify) 
	{
		if(notify == true)
		{
			monthLastAccess = month;
			yearLastAccess = year;
			setChanged();
			notifyObservers(new String("BALANCE: " + this.id +" on " + month + "/" + year));
		}
		return amount;
	}
	
	public int getAccoundId() {
		return id;
	}
	
	public void setDateBegin(int month, int year)
	{
		monthBegin = month;
		yearBegin = year;
	}
	
	public int getMonthBegin()
	{
		return monthBegin;
	}
	
	public int getYearBegin()
	{
		return yearBegin;
	}
	
	public int getMonthLastAccess()
	{
		return monthLastAccess;
	}
	
	public int getYearLastAccess()
	{
		return yearLastAccess;
	}
	
	public boolean equals(Object o)
	{
		if(o instanceof Account)
			if(((Account) o).getAccoundId() == id)
				return true;
		return false;
	}
	
}
