package model;

import javax.swing.JOptionPane;

public class SavingsAccount extends Account{
	
	private boolean depositStatus;
	private final static double yearInterestRate = 0.8;
	
	public SavingsAccount(int id, int month, int year)
	{
		super(id, month, year);
		depositStatus = false;
	}
	
	
	public boolean deposit(double sum, int month, int year) {
		if(depositStatus == false)
		{
			super.deposit(sum, month, year);
			super.setDateBegin(month, year);
			depositStatus = true;
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Money are already introduced in the savings account!");
		}
		return depositStatus;
	}

	public double redraw(double sum, int month, int year) {
		
		if(sum == getBalance(month, year, false) && depositStatus == true)
		{
			super.redraw(super.getBalance(month, year, false), month, year);
			depositStatus = false;
			return sum;
		}
		
		return 0;
	}
	

	public double getBalance(int month, int year, boolean notify) {
		return super.getBalance(month, year, notify) + getInterestAmount(month, year);
	}

	public boolean canRedraw()
	{
		return depositStatus;
	}
	
	private double getInterestAmount(int month, int year)
	{
		return super.getBalance(month, year, false) * yearInterestRate * ((year - super.getYearBegin()) * 12 + month - super.getMonthBegin()) / 12;
	}
	
	public String toString()
	{
		return "SavAcc with id:" + super.getAccoundId();
	}
}
