package HW4.HW4;

import java.util.List;

import bank.Bank;
import junit.framework.TestCase;
import model.*;

public class Testing extends TestCase {
	protected Person p1, p2, p3, p4;
	protected Bank b;
	
	protected void setUp()
	{
		p1 = new Person(0, "Piscu Vlad", "0752453123");
		p2 = new Person(1, "Piscu Andra", "0723123456");
		p3 = new Person(2, "Detesan Claudiu", "0744120987");
		p4 = null;
		b = new Bank();
	}
	
	
	
	public void testWrongAddPerson()
	{
		b.addPerson(p4);
		
	}
	
	/*public void testWrightAddPerson()
	{
		b.addPerson(p1);
		b.addPerson(p2);
		b.addPerson(p3);
	}
	
	public void testAddingAccounts()
	{
		b.addPerson(p1);
		b.addPerson(p2);
		b.addNewSavingsAccount(p1, 1, 1998);
		b.addNewSpendingAcound(p1, 2, 1999, 5);
		b.addNewSpendingAcound(p2, 2, 2000, 5);
		assertTrue(b.accountsToList(p1).size() == 2);
		assertTrue(b.accountsToList(p2).size() == 1);
	}
	
	public void testRemoveAccountsAndPerson()
	{
		b.addPerson(p1);
		b.addPerson(p2);
		b.addNewSavingsAccount(p1, 1, 1998);
		b.addNewSpendingAcound(p1, 2, 1999, 5);
		b.addNewSpendingAcound(p2, 2, 2000, 5);
		b.addNewSpendingAcound(p2, 2, 2001, 5);
		b.deleteAccount(b.accountsToList(p2).get(1), p1);
		assertTrue(b.accountsToList(p1).size() == 1);
		b.removePerson(p2);
		assertFalse(b.personExists(p2));
	}
	
	public void testDepositAndRedraw()
	{
		b.addPerson(p1);
		b.addNewSavingsAccount(p1, 1, 1998);
		b.addNewSpendingAcound(p1, 2, 1999, 5);
		List<Account> a = b.accountsToList(p1);
		a.get(0).deposit(1500, 3, 1999);
		System.out.println(p1.getLastOperation());
		a.get(1).deposit(350, 2, 2000);
		System.out.println(p1.getLastOperation());
		a.get(1).deposit(650, 3, 2000);
		System.out.println(p1.getLastOperation());
		assertTrue(a.get(0).redraw(a.get(0).getBalance(3, 2000, false), 3, 2000) == 2700);
		System.out.println(p1.getLastOperation());
		assertTrue(a.get(1).redraw(500, 7, 2001) == 505);
		System.out.println(p1.getLastOperation());
		assertTrue(a.get(1).redraw(700, 7, 2001) == 0);
		System.out.println(p1.getLastOperation());
	}*/
	
	public void testALL()
	{
		b.addPerson(p1);
		b.addPerson(p2);
		b.addPerson(p3);
		b.addNewSavingsAccount(p1, 1, 1998);
		b.addNewSpendingAcound(p1, 2, 1999);
		b.addNewSpendingAcound(p2, 2, 2000);
		b.addNewSpendingAcound(p2, 2, 2001);
		b.deleteAccount(b.accountsToList(p2).get(1), p2);
		assertTrue(b.accountsToList(p2).size() == 1);
		List<Account> a = b.accountsToList(p1);
		a.get(0).deposit(1500, 3, 1999);
		System.out.println(p1.getLastOperation());
		a.get(1).deposit(350, 2, 2000);
		System.out.println(p1.getLastOperation());
		a.get(1).deposit(650, 3, 2000);
		System.out.println(p1.getLastOperation());
		assertTrue(a.get(0).redraw(a.get(0).getBalance(3, 2000, false), 3, 2000) == 2700);
		System.out.println(p1.getLastOperation());
		assertTrue(a.get(1).redraw(500, 7, 2001) == 505);
		System.out.println(p1.getLastOperation());
		assertTrue(a.get(1).redraw(700, 7, 2001) == 0);
		System.out.println(p1.getLastOperation());
		b.removePerson(p1);
		assertFalse(b.personExists(p1));
		b.removePerson(p2);
		assertFalse(b.personExists(p2));
		b.removePerson(p3);
		assertFalse(b.personExists(p3));
	}
}
